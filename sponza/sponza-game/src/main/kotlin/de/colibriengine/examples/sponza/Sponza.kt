package de.colibriengine.examples.sponza

import de.colibriengine.ColibriApplication
import de.colibriengine.ColibriEngine
import de.colibriengine.input.FECommands
import de.colibriengine.input.JoystickInputCallbacks
import de.colibriengine.input.MouseKeyboardInputCallbacks
import de.colibriengine.util.Version
import org.koin.core.component.KoinComponent
import org.koin.core.component.get

class Sponza : KoinComponent, ColibriApplication, MouseKeyboardInputCallbacks, JoystickInputCallbacks {

    private val appName = "Sponza - v.${Version(0, 0, 0, 9)}"

    private val engine: ColibriEngine = get<ColibriEngine>().apply {
        inputManager.addMKInputCallback(this@Sponza)
        inputManager.addJInputCallback(this@Sponza)
        terminateWithoutWindows = true
        windowManager.onlyProcessActiveWindow = false
    }

    init {
        // Method blocks until the engine terminates.
        engine.start(this)
    }

    override fun onAppStart() {
        engine.workerManager.addWorker(FeatureShowroomWorker(engine))
    }

    override fun onAppLoopStarted() {}
    override fun onAppLoopStopped() {}
    override fun onAppUpdate() {}
    override fun onAppTermination() {}

    override fun mkKeyCallback(key: Int, scancode: Int, action: Int, mods: Int) {
        if (key == FECommands.CLOSE_COMMAND.kKey && action == FECommands.CLOSE_COMMAND.kKeyAction) {
            engine.shouldTerminate = true
        }
        if (key == FECommands.TOGGLE_FULLSCREEN.kKey && action == FECommands.TOGGLE_FULLSCREEN.kKeyAction) {
            engine.windowManager.activeWindow!!.toggleFullscreen()
        }
    }

    override fun mkMoveCallback(dx: Int, dy: Int) {}
    override fun mkButtonCallback(button: Int, action: Int, mods: Int) {}
    override fun jAxisCallback(axis: Int, axisValue: Float) {}
    override fun jButtonCallback(button: Int, action: Int) {}
    override fun jLostConnectionCallback(index: Int) {}

}
