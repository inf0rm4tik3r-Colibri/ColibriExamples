package de.colibriengine.examples.sponza.scenes

import de.colibriengine.ColibriEngine
import de.colibriengine.ENGINE_NAME
import de.colibriengine.asset.ResourceName
import de.colibriengine.audio.*
import de.colibriengine.graphics.camera.CameraFactoryImpl
import de.colibriengine.graphics.camera.concrete.ActionCameraImpl
import de.colibriengine.graphics.font.BitmapFont
import de.colibriengine.graphics.font.Text3D
import de.colibriengine.graphics.font.TextFactoryImpl
import de.colibriengine.graphics.font.TextMode
import de.colibriengine.graphics.icons.AudioIcon
import de.colibriengine.graphics.icons.LightIcon
import de.colibriengine.graphics.lighting.light.DirectionalLight
import de.colibriengine.graphics.lighting.light.LightFactory
import de.colibriengine.graphics.lighting.light.LightFactoryImpl
import de.colibriengine.graphics.lighting.light.PointLight
import de.colibriengine.graphics.material.MaterialDefinition
import de.colibriengine.graphics.material.MaterialImpl
import de.colibriengine.graphics.material.MaterialTextureDefinition
import de.colibriengine.graphics.model.ModelNode
import de.colibriengine.graphics.model.concrete.Icosphere
import de.colibriengine.graphics.window.Window
import de.colibriengine.math.vector.vec3f.StdVec3f
import de.colibriengine.math.vector.vec3f.StdVec3fFactory.Companion.UNIT_X_AXIS
import de.colibriengine.math.vector.vec3f.StdVec3fFactory.Companion.UNIT_Y_AXIS
import de.colibriengine.math.vector.vec4f.StdVec4fFactory
import de.colibriengine.scene.AbstractScene
import de.colibriengine.scene.graph.AbstractSceneGraphNode
import de.colibriengine.ui.GUI
import de.colibriengine.ui.UIBuilder
import de.colibriengine.ui.UIComponentFactoryImpl
import de.colibriengine.ui.components.Canvas
import de.colibriengine.ui.structure.anchoring.AnchorPosition.BOTTOM_RIGHT
import de.colibriengine.ui.structure.anchoring.AnchorPosition.TOP_LEFT
import de.colibriengine.ui.widgets.FrameInfo
import de.colibriengine.util.PhysicalUnit.PX
import de.colibriengine.util.Timer
import org.koin.core.component.KoinComponent
import org.koin.core.component.get
import kotlin.math.cos
import kotlin.math.sin

class TestScene(private val engine: ColibriEngine, private val window: Window) :
    AbstractScene(engine.ecs.createEntity()), KoinComponent {

    val worldCamera: ActionCameraImpl
    val worldCamera2: ActionCameraImpl

    private val sponza: ModelNode
    private val sphere: ModelNode

    private val trackFactory: AudioTrackFactory = get()
    private val sourceFactory: AudioSourceFactory = get()
    private val listener: AudioListener = get()
    private val fireSound1: Audio3D
    private val fireSound2: Audio3D
    private val fireSound3: Audio3D
    private val fireSound4: Audio3D
    private val music: Audio2D

    private val font32: BitmapFont
    private val font256: BitmapFont
    private val textEntity: Text3D
    private val pointLight1: PointLight
    private val pointLight2: PointLight
    private val pointLight3: PointLight
    private val directionalLight: DirectionalLight

    private lateinit var gui: GUI
    private lateinit var canvas: Canvas
    private lateinit var frameInfo: FrameInfo

    private val textFactory = TextFactoryImpl(window.modelManager.modelFactory.meshFactory)

    private val lightFactory: LightFactory = LightFactoryImpl(
        engine.ecs,
        engine.windowManager,
        LightIcon(window.modelManager.modelFactory, window.textureManager)
    )

    init {
        val ecs = engine.ecs
        val modelManager = window.modelManager
        val meshFactory = modelManager.modelFactory.meshFactory

        add(
            ModelNode(
                ecs.createEntity(), modelManager.modelFactory.coordinateSystem(
                    StdVec3f().set(0f, 0f, 0f), 0f, 6f, 0.5f, 5.0f
                )
            )
        )
        sponza = ModelNode(ecs.createEntity(), modelManager.requestModel("models/sponza2/sponza.obj"))
        sponza.transform.translation.set(0f, 0f, 0f)
        sponza.transform.scale.set(0.007f)
        add(sponza)

        // TODO: Create with modelFactory!
        val icosphere = Icosphere(1f, 3, meshFactory)
        val tex = MaterialTextureDefinition()
        tex.texturePath = "textures/raster.png"
        val materialDefinition = MaterialDefinition()
        materialDefinition.ambientColorTexture = tex
        val material = MaterialImpl(materialDefinition) // TODO: User MaterialManager instead.
        material.loadTextures(engine.windowManager.activeWindow!!.textureManager, "")
        icosphere.meshes[0].material = material
        sphere = ModelNode(ecs.createEntity(), icosphere)
        sphere.transform.setTranslation(0f, 3f, 0f)
        add(sphere)

        val audioIcon = AudioIcon(meshFactory).createMaterial(window.textureManager)

        val fireSoundTrack = trackFactory.get(ResourceName("audio/spa_fire_burning_firepit_16bit_48khz_mono.wav"))

        fireSound1 = Audio3D(ecs, audioIcon, engine.windowManager, sourceFactory.createSource3D {
            audioTrack = fireSoundTrack
            looping = true
            position.set(-4.25f, 1f, -1.4f)
            rolloffFactor = 2f
            play()
        })
        add(fireSound1)

        fireSound2 = Audio3D(ecs, audioIcon, engine.windowManager, sourceFactory.createSource3D {
            audioTrack = fireSoundTrack
            looping = true
            position.set(-4.25f, 1f, 1f)
            rolloffFactor = 2f
            play()
        })
        add(fireSound2)

        fireSound3 = Audio3D(ecs, audioIcon, engine.windowManager, sourceFactory.createSource3D {
            audioTrack = fireSoundTrack
            looping = true
            position.set(3.6f, 1f, -1.4f)
            rolloffFactor = 2f
            play()
        })
        add(fireSound3)

        fireSound4 = Audio3D(ecs, audioIcon, engine.windowManager, sourceFactory.createSource3D {
            audioTrack = fireSoundTrack
            looping = true
            position.set(3.6f, 1f, 1f)
            rolloffFactor = 2f
            play()
        })
        add(fireSound4)

        val musicTrack = trackFactory.get(ResourceName("audio/bio_unit_idiophone.wav"))
        val musicSource = sourceFactory.createSource2D()
        musicSource.audioTrack = musicTrack
        musicSource.volume = 0.4f
        musicSource.looping = true
        musicSource.play()
        music = Audio2D(ecs, audioIcon, engine.windowManager, musicSource)
        music.transform.translation.set(0f, 2f, 0f)
        add(music)

        val fontManager = engine.fontManager
        font32 = fontManager.requestFont(ResourceName("fonts/jetbrains_32.bin.fnt"))
        font256 = fontManager.requestFont(ResourceName("fonts/jetbrains_256_2048.bin.fnt"))

        textEntity = Text3D(ecs.createEntity(), textFactory, TextMode.FIXED, 40u, font32)
        textEntity.text.update(ENGINE_NAME)
        textEntity.transform.setTranslation(-6f, 8.5f, -1.7f).scale.set(0.03f)
        add(textEntity)

        directionalLight = lightFactory.directional()
        directionalLight.orientation.initRotation(UNIT_X_AXIS, 73f)
        directionalLight.transform.rotate(
            UNIT_Y_AXIS,
            40f
        ) // TODO: use orientation instead? place rotate() in QuaternionF.
        directionalLight.position.set(0f, 8f, 2f)
        directionalLight.color.set(3.0f, 3.0f, 3.0f)
        directionalLight.castsShadows = true
        //directionalLight.getVarianceShadowMap().setSettings()
        add(directionalLight)

        pointLight1 = lightFactory.point()
        pointLight1.position.set(4.5f, 4.5f, 0.0f)
        pointLight1.color.set(0.5f, 0.5f, 0.5f)
        pointLight1.attenuation.set(0.3f, 0.2f, 1f)
        pointLight1.dropOffFactor = 500f
        add(pointLight1 as AbstractSceneGraphNode)

        pointLight2 = lightFactory.point()
        pointLight2.position.set(-3.0f, 2.0f, 1.4f)
        pointLight2.color.set(0.8f, 0.3f, 0.2f)
        pointLight2.attenuation.set(0.22f, 0.2f, 1f)
        add(pointLight2)

        pointLight3 = lightFactory.point()
        pointLight3.position.set(-3.0f, 2.0f, -1.4f)
        pointLight3.color.set(0.8f, 0.3f, 0.2f)
        pointLight3.attenuation.set(0.22f, 0.2f, 1f)
        add(pointLight3)

        // addLights()

        worldCamera = ActionCameraImpl(ecs.createEntity(), window)
        worldCamera.transform.setTranslation(-3f, 1.0f, 1.2f)
        worldCamera.transform.rotation.initRotation(-20f, 45f, 0f)
        worldCamera.use()
        // TODO: cameras are no longer added to an InputManager automatically... We should fix that!
        engine.inputManager.addMKInputCallback(worldCamera)
        engine.inputManager.addJInputCallback(worldCamera)
        add(worldCamera)

        worldCamera2 = ActionCameraImpl(ecs.createEntity(), window)
        // TODO: cameras are no longer added to an InputManager automatically... We should fix that!
        engine.inputManager.addMKInputCallback(worldCamera2)
        engine.inputManager.addJInputCallback(worldCamera2)
        add(worldCamera2)

        createGUI(engine)
    }

    fun createGUI(engine: ColibriEngine) {
        gui = GUI(
            engine.ecs,
            CameraFactoryImpl(engine.ecs, window),
            { engine.currentRenderer?.renderOptions?.guiResolution?.x ?: 0 },
            { engine.currentRenderer?.renderOptions?.guiResolution?.y ?: 0 }
        )
        canvas = Canvas(engine.ecs.createEntity(), gui)

        val uiComponentFactory = UIComponentFactoryImpl(
            gui,
            engine.ecs,
            textFactory,
            window.materialManager.materialFactory,
            window.modelManager.modelFactory
        )
        val uiBuilder = UIBuilder(uiComponentFactory)

        frameInfo = FrameInfo(uiComponentFactory, font32)
        frameInfo.anchor(TOP_LEFT, canvas, TOP_LEFT)
        frameInfo.anchor(BOTTOM_RIGHT, canvas, BOTTOM_RIGHT)
        frameInfo.width(330f, PX)
        frameInfo.height(280f, PX)
        frameInfo.marginTop(25f, PX)
        // frameInfo.marginBottom(25f, PX)
        frameInfo.marginLeft(25f, PX)
        // frameInfo.marginRight(25f, PX)
        canvas.add(frameInfo)

        // Add the canvas to the GUI instance.
        gui.rootNode.add(canvas)
        guis.add(gui)
    }

    override fun destroyScene() {
        fireSound1.destroy()
        fireSound2.destroy()
        fireSound3.destroy()
        fireSound4.destroy()
        music.destroy()
    }

    override fun init() {}
    private var i = 0f
    override fun update(timing: Timer.View) {
        engine.activeCameraOfActiveWindow?.apply {
            listener.position.set(position)
            listener.orientation.set(lookRotation)
        }

        i += 2 * timing.deltaSP()
        sphere.transform.setTranslation(
            sin(i.toDouble()).toFloat(), 5f, cos(i.toDouble()).toFloat()
        )

        // TODO: move somewhere else..
        canvas.x(0f)
        canvas.y(engine.currentRenderer?.renderOptions?.guiResolution?.y?.toFloat() ?: 0f)
        canvas.z(0f)
        canvas.width(engine.currentRenderer?.renderOptions?.guiResolution?.x?.toFloat() ?: 0f, PX)
        canvas.height(engine.currentRenderer?.renderOptions?.guiResolution?.y?.toFloat() ?: 0f, PX)
        canvas.depth(1f, PX)

        gui.computeLayout()

        val debug = engine.debugRenderer!!
        debug.point(StdVec3f(1f, 1f, 0f), StdVec4fFactory.RED, 20f)
        debug.point(StdVec3f(2f, 2f, 0f), StdVec4fFactory.GREEN, 30f)
        debug.point(StdVec3f(3f, 3f, 0f), StdVec4fFactory.BLUE, 40f)
        debug.line(
            from = StdVec3f(0f, 0f, 0f),
            to = StdVec3f().set(sphere.transform.translation).minus(0f, 1f, 0f),
            StdVec4fFactory.WHITE,
            20f
        )
    }

    override fun destroy() {
        throw RuntimeException("destroy")
    }

}
