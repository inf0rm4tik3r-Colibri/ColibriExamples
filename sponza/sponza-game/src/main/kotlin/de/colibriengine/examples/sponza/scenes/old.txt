FROM createGUI()

        /*
        button = Button(EntityId(engine.idGenerator.generate()), gui)
        button.anchor(TOP_LEFT, canvas, TOP_LEFT)
        button.anchor(BOTTOM_RIGHT, canvas, BOTTOM_RIGHT)
        button.marginTop(90f, PERCENT)
        button.marginLeft(15f, PERCENT)
        button.marginRight(50f, PERCENT)
        canvas.add(button)

        buttonBackground = Background(engine, gui)
        buttonBackground.anchor(TOP_LEFT, button, TOP_LEFT)
        buttonBackground.anchor(BOTTOM_RIGHT, button, BOTTOM_RIGHT)
        buttonBackground.useImage(false)
        button.add(buttonBackground)

        val button2 = Button(EntityId(engine.idGenerator.generate()), gui)
        button2.anchor(AnchorPosition.BOTTOM_LEFT, button, AnchorPosition.TOP_RIGHT)
        button2.width(150f, PhysicalUnit.PX)
        button2.height(75f, PhysicalUnit.PX)
        canvas.add(button2)

        buttonBackground2 = Background(engine, gui)
        buttonBackground2.anchor(AnchorPosition.TOP_LEFT, button2, AnchorPosition.TOP_LEFT)
        buttonBackground2.anchor(AnchorPosition.BOTTOM_RIGHT, button2, AnchorPosition.BOTTOM_RIGHT)
        button2.add(buttonBackground2)


        buttonText = Text2D(EntityId(engine.idGenerator.generate()), 30, gui)
        buttonText.textComponent2D.setShader(basicShader).setFont(font)
        buttonText.textComponent2D.update("Hallo Welt")
        buttonText.anchor(TOP_LEFT, button, TOP_LEFT)
        //buttonText.anchor(BOTTOM_RIGHT, button, BOTTOM_RIGHT)
        buttonText.width(1f, PX)
        buttonText.height(1f, PX)
        button.add(buttonText)
        */


FROM init()

        /*
        add(ModelComponent.fromResource(
            engine.getIdGenerator().genID(),
            window.getModelManager(), "models/sword.fbx",
            basicShader, basicShadowGenerationShader
        ))
        */

        //animatedTriangle = new AnimatedTriangle(engine.getIdGenerator().genID(), basicShader, engine.getTimer())
        //add(animatedTriangle)
        //engine.getWindowManager().getActiveWindow().getModelManager().addModel() // TODO allow add* to manager..

        /*
        texturedTriangle = new TexturedTriangle(engine.getIdGenerator().genID(), window
            .getTextureManager(), "textures/wall.jpg", basicShader, engine.getTimer())
        texturedTriangle.getTransform().setTranslation(0, 0, -6)
        add(texturedTriangle)

        photosynthesisBoard = new PhotosynthesisBoard(engine.getIdGenerator().genID(), window
            .getTextureManager(), basicShader, engine.getTimer())
        photosynthesisBoard.getTransform().setTranslation(0, 0, 0).getScale().set(20.0f)
        add(photosynthesisBoard)


        */
        /*
        dragon = Dragon.newInstance(
            engine.getIdGenerator().genID(), window.getModelManager(),
            basicShader, basicShadowGenerationShader
        )
        dragon.getTransform().getScale().set(0.1f)
        add(dragon)
        */

        ground = TexturedQuad(ecs.createEntity(), window.textureManager, "textures/raster.png")
        ground.transform.setTranslation(0f, 0f, 0f)
        ground.transform.rotation.initRotation(UNIT_X_AXIS, 0.0f)
        ground.transform.scale.set(15f)
        //add(ground)
        plane = TexturedQuad(ecs.createEntity(), window.textureManager, "textures/raster.png")
        plane.transform.translation.set(0f, 5f, -5f)
        plane.transform.rotation.initRotation(UNIT_X_AXIS, 90.0f)
        plane.transform.scale.set(10f)
        //add(plane)

        /*
        directionalLight2 = new DirectionalLight(engine, window
            .getTextureManager(), basicTransparencyFilterShader, varianceShadowMapGenerationShader)
        directionalLight2.getTransform().setRotation(ImmutableVec3f.UNIT_X_AXIS.view, 116)
        directionalLight2.getTransform().setTranslation(0, 8, 2)
        directionalLight2.getColor().set(0.0f, 1.0f, 0.1f)
        directionalLight2.setCastsShadows(false)
        add(directionalLight2)
        */




    private fun addLights() {
        for (i in 0..20) {
            val pointLight = engine.lightFactory.point()
            pointLight.position.set(random.nextFloat(-8f, 8f), random.nextFloat(0f, 6f), random.nextFloat(-3f, 3f))
            pointLight.color.set(random.nextFloat(0f, 1f), random.nextFloat(0f, 1f), random.nextFloat(0f, 1f))
            pointLight.attenuation.set(random.nextFloat(0.5f, 1f), random.nextFloat(0.5f, 1f), 1f)
            pointLight.dropOffFactor = random.nextInt(300, 600).toFloat()
            add(pointLight)
        }
    }
