package de.colibriengine.examples.sponza

import de.colibriengine.ColibriEngine
import de.colibriengine.examples.sponza.scenes.TestScene
import de.colibriengine.input.FECommands
import de.colibriengine.input.JoystickInputCallbacks
import de.colibriengine.input.MouseKeyboardInputCallbacks
import de.colibriengine.logging.LogUtil.getLogger
import de.colibriengine.worker.concrete.AbstractSimpleWorker
import org.lwjgl.glfw.GLFW

class FeatureShowroomWorker(engine: ColibriEngine) :
    AbstractSimpleWorker(engine), MouseKeyboardInputCallbacks, JoystickInputCallbacks {

    private lateinit var scene: TestScene

    override fun onAdd() {
        super.onAdd()

        scene = TestScene(engine, window)
        setCurrentScene(scene)
    }

    override fun mkKeyCallback(key: Int, scancode: Int, action: Int, mods: Int) {
        //if (windowId == scene.window.id) {
        if (key == FECommands.CLOSE_COMMAND.kKey && action == FECommands.CLOSE_COMMAND.kKeyAction) {
            engine.shouldTerminate = true
        }
        if (key == GLFW.GLFW_KEY_1) {
            scene.worldCamera.use()
        }
        if (key == GLFW.GLFW_KEY_2) {
            //scene.worldCamera2.use()
        }
        if (key == GLFW.GLFW_KEY_1 && action == GLFW.GLFW_PRESS) {
            renderer.ppEnabled = !renderer.ppEnabled
        }
        //}
    }

    var mx: Int = 0
    var my: Int = 0

    override fun mkPosCallback(x: Int, y: Int) {
        mx = x
        my = y
    }

    override fun mkMoveCallback(dx: Int, dy: Int) {}

    override fun mkButtonCallback(button: Int, action: Int, mods: Int) {
        if (action == GLFW.GLFW_PRESS) {
            LOG.info("click")
            val pi = renderer.readPickingInfo(mx, my)
            LOG.info(pi)
        }
    }

    override fun jAxisCallback(axis: Int, axisValue: Float) {}
    override fun jButtonCallback(button: Int, action: Int) {}
    override fun jLostConnectionCallback(index: Int) {}

    companion object {
        private val LOG = getLogger(this)
    }

}
