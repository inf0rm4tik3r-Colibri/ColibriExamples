package de.colibriengine.examples.sponza

import de.colibriengine.startColibri

fun main(args: Array<String>) = startColibri(withArguments = args, awaitingUserInputToStart = false) {
    Sponza()
}
