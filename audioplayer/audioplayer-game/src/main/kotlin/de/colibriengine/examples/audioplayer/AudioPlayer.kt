package de.colibriengine.examples.audioplayer

import de.colibriengine.ColibriApplication
import de.colibriengine.ColibriEngine
import de.colibriengine.asset.ResourceName
import de.colibriengine.audio.AudioListener
import de.colibriengine.audio.AudioMaster
import de.colibriengine.audio.AudioSourceFactory
import de.colibriengine.audio.AudioTrackFactory
import de.colibriengine.input.FECommands
import de.colibriengine.input.JoystickInputCallbacks
import de.colibriengine.input.MouseKeyboardInputCallbacks
import de.colibriengine.logging.LogUtil.getLogger
import de.colibriengine.util.Version
import org.koin.core.component.KoinComponent
import org.koin.core.component.get

class AudioPlayer : KoinComponent, ColibriApplication, MouseKeyboardInputCallbacks, JoystickInputCallbacks {

    private val appName = "AudioPlayer - v.${Version(0, 0, 0, 1)}"
    private val engine: ColibriEngine = get()

    init {
        val audioMaster: AudioMaster = get<AudioMaster>().also { it.initAudioSystem() }
        val trackFactory: AudioTrackFactory = get()
        val sourceFactory: AudioSourceFactory = get()
        val listener: AudioListener = get()

        val fireMono = trackFactory.get(ResourceName("spa_fire_burning_firepit_16bit_48khz_mono.wav"))
        val ambientStereo = trackFactory.get(ResourceName("bio_unit_idiophone.wav"))

        listener.position.set(0f, 0f, 0f)

        val musicSource = sourceFactory.createSource2D()
        musicSource.volume = 1f
        musicSource.looping = true
        musicSource.audioTrack = ambientStereo
        musicSource.play()

        val fireSource = sourceFactory.createSource3D()
        fireSource.position.set(1f, 0f, 2f)
        fireSource.volume = 2f
        fireSource.looping = true
        fireSource.audioTrack = fireMono
        fireSource.play()

        while (musicSource.isPlaying() or fireSource.isPlaying()) {
            LOG.info("Playing... Press q to quit:")
            val read = readLine()
            if ((read != null) and (read == "q")) {
                break
            }
        }

        musicSource.stop()
        musicSource.unload()
        fireSource.stop()
        fireSource.unload()

        audioMaster.destroyAudioSystem()
    }

    override fun onAppStart() {}
    override fun onAppLoopStarted() {}
    override fun onAppLoopStopped() {}
    override fun onAppUpdate() {}
    override fun onAppTermination() {}

    override fun mkKeyCallback(key: Int, scancode: Int, action: Int, mods: Int) {
        if (key == FECommands.CLOSE_COMMAND.kKey && action == FECommands.CLOSE_COMMAND.kKeyAction) {
            engine.shouldTerminate = true
        }
        if (key == FECommands.TOGGLE_FULLSCREEN.kKey && action == FECommands.TOGGLE_FULLSCREEN.kKeyAction) {
            engine.windowManager.activeWindow!!.toggleFullscreen()
        }
    }

    override fun mkMoveCallback(dx: Int, dy: Int) {}
    override fun mkButtonCallback(button: Int, action: Int, mods: Int) {}
    override fun jAxisCallback(axis: Int, axisValue: Float) {}
    override fun jButtonCallback(button: Int, action: Int) {}
    override fun jLostConnectionCallback(index: Int) {}

    companion object {
        private val LOG = getLogger(this)
    }

}
