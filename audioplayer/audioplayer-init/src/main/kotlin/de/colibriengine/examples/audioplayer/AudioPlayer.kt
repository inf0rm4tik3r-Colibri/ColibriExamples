package de.colibriengine.examples.audioplayer

import de.colibriengine.startColibri

fun main(args: Array<String>) = startColibri(withArguments = args, awaitingUserInputToStart = false) {
    AudioPlayer()
}
