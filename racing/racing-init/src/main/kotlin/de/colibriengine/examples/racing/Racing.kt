package de.colibriengine.examples.racing

import de.colibriengine.startColibri

fun main(args: Array<String>) = startColibri(withArguments = args, awaitingUserInputToStart = false) {
    Racing()
}
