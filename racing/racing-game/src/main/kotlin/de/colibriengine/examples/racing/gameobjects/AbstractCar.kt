package de.colibriengine.examples.racing.gameobjects

import de.colibriengine.ecs.Entity
import de.colibriengine.math.quaternion.quaternionF.QuaternionF
import de.colibriengine.math.quaternion.quaternionF.StdQuaternionF
import de.colibriengine.math.vector.vec3d.StdVec3d
import de.colibriengine.math.vector.vec3d.Vec3d
import de.colibriengine.math.vector.vec3d.Vec3dAccessor
import de.colibriengine.math.vector.vec3f.StdVec3f
import de.colibriengine.math.vector.vec3f.Vec3f
import de.colibriengine.scene.graph.AbstractSceneGraphNode
import de.colibriengine.util.Timer

/**
 * Key aspects:
 * - Handle the longitudinal and lateral forces separately.
 * - Longitudinal forces:
 * - wheel force
 * - braking force
 * - rolling resistance
 * - drag (= air-resistance)
 */
// TODO: Catch user input and update *PedalPosition's, handBreakPosition, steering
abstract class AbstractCar(entity: Entity) : AbstractSceneGraphNode(entity) {

    var world: World? = null

    /** The total mass of the car in [kg]. */
    var mass = 0f

    /**
     * The center of gravity of the car:
     * x - determines the longitudinal position
     * y - determines the lateral position
     * z - determines the height of the cog
     * Use values in the range [-1, 1],
     * where 0 means centered, 1 shifts to left or front, and -1 shifts to right or rear.
     */
    var centerOfGravity: StdVec3f? = null

    /** The distance between the two axel's. */
    var wheelbase = 0f

    /** The radius of a wheel. Center to tire edge (road contact) in [m]. */
    var wheelRadius = 0f

    /** The amount of forward gears the transmission supports. */
    var forwardGears = 0

    /** The ratios of the forward gears. */
    var forwardGearRatios: FloatArray = floatArrayOf()

    /** The amount of reverse gears the transmission supports. */
    var reverseGears = 0

    /** The ratios of the reverse gears. */
    var reverseGearRatios: FloatArray = floatArrayOf()

    /** The size of the frontal are of the car in [m^2]. (An example value might be 2.2f) */
    var frontalArea = 0f
    var enginePower = 0f

    /** The power of the breaks. Should be in range [0, 1]. */
    var cBreakPower = 0f

    /**
     * Car-dependant constant usually determined in wind tunnels. Used to estimate the [AbstractCar.cDrag] constant. (An
     * example value might be 0.3f)
     */
    var cDragFriction = 0f

    /** Constant with which to compute the drag (air-resistance) for the current speed. */
    var cDrag = 0f

    /**
     * Constant with which to compute the rolling resistance for the current speed. Should be approximately 30 times the
     * value of [AbstractCar.cDrag]. **See** [AbstractCar.calculateRollingResistance].
     */
    var cRollingResistance = 0f

    /** Whether the cars engine is running. */
    var isRunning = false

    /** Position of the clutch pedal: Range [0, 1] (0 = released) */
    var clutchPedalPosition = 0f

    /** Position of the break pedal: Range [0, 1] */
    var breakPedalPosition = 0f

    /** Position of the gas pedal: Range [0, 1] */
    var gasPedalPosition = 0f

    /** Position of the hand break: Range [0, 1] (0 = released) */
    var handBreakPosition = 0f

    /**
     * The steering wheel position is used to determine how the car is going to turn. Must always be in the range [-1,
     * 1].
     *
     *      -1: If the steering wheel is completely turned to the left.
     *       0: Initial position (steering wheel centered, wheels point forward)
     *      +1: If the steering wheel is completely turned to the right.
     */
    var steeringWheelPosition = 0f
        set(value) {
            require(!(value < -1f || value > 1f)) {
                "A steeringWheelPosition in range [-1, 1] must be provided!"
            }
            field = value
        }


    /** The current gear in which the transmission is. */
    var gear = 0

    /* LIVE DATA */
    var position: Vec3d = StdVec3d()
    var velocity: Vec3d = StdVec3d()
    var acceleration: Vec3d = StdVec3d()
    var longitudinalForce: Vec3d = StdVec3d()
    var breakingForce: Vec3d = StdVec3d()

    /** Traction (force delivered by the engine via the rear wheels). */
    var engineTraction: Vec3d = StdVec3d()
    var rollingResistance: Vec3d = StdVec3d()
    var drag: Vec3d = StdVec3d()

    /* TEMPORARY */
    private val fwdDir: Vec3d = StdVec3d()
    private val fwdDirF: Vec3f = StdVec3f()
    private val bwdDir: Vec3d = StdVec3d()
    private val bwdDirF: Vec3f = StdVec3f()
    private val upDir: Vec3d = StdVec3d()
    private val upDirF: Vec3f = StdVec3f()
    private val velocityDelta: Vec3d = StdVec3d()
    private val positionDelta: Vec3d = StdVec3d()
    private val tmpVec3f: Vec3f = StdVec3f()
    private val tmpQuaternionF1: QuaternionF = StdQuaternionF()
    private val tmpQuaternionF2: QuaternionF = StdQuaternionF()

    /** Calculates the new forces for the current frame. */
    fun updateCarData(timing: Timer.View) {
        estimateCDrag()
        calculateDrag()
        estimateCRollingResistance()
        calculateRollingResistance()

        //getTransform().getRotation().forward(fwdDirF);
        transformCalculation.forward(fwdDirF)
        fwdDir.set(fwdDirF)
        transform.rotation.backward(bwdDirF)
        bwdDir.set(bwdDirF)
        transformCalculation.up(upDirF)
        upDir.set(upDirF)
        calculateEngineTraction(fwdDir)
        calculateBreakingForce(fwdDir)
        calculateLongitudinalForce()
        calculateAcceleration()
        updateVelocity(timing)
        updatePosition(timing)
        updateOrientation(timing)
    }

    private fun updateVelocity(timing: Timer.View) {
        velocity.plus(velocityDelta.set(acceleration).times(timing.delta()))
    }

    private fun updatePosition(timing: Timer.View) {
        transform.translation.set(
            position.plus(positionDelta.set(velocity).times(timing.delta()))
        )
    }

    private fun updateOrientation(timing: Timer.View) {
        transform.rotate(
            tmpQuaternionF1.initRotation( //upDirF,
                transformCalculation.up(tmpVec3f),
                -20 * steeringWheelPosition * timing.deltaSP()
            )
        )
    }

    /** Updates the current acceleration of the car in [m/s^2]. */
    private fun calculateAcceleration() {
        acceleration
            .set(longitudinalForce)
            .div(mass.toDouble())
    }

    private fun calculateLongitudinalForce() {
        longitudinalForce
            .set(engineTraction) // Can accelerate and
            .minus(breakingForce) // brake at the same time!
            .minus(drag)
            .minus(rollingResistance)
    }

    private fun calculateBreakingForce(carDirUnit: Vec3dAccessor) {
        breakingForce.set(carDirUnit).times(cBreakPower.toDouble()).times(breakPedalPosition.toDouble())
    }

    /** @param carDirUnit A unit vector of the direction the car is heading. */
    private fun calculateEngineTraction(carDirUnit: Vec3dAccessor) {
        engineTraction.set(carDirUnit).times(enginePower.toDouble())
            .times(gasPedalPosition.toDouble()) //TODO: calc torque!!
    }

    /**
     * The force of rolling resistance is caused by friction between the rubber and road surface as the wheels roll
     * along, as well as friction in the axles, etc etc.. We will approximate this with a force that's proportional to
     * the velocity using another constant ([AbstractCar.cRollingResistance]). Pointing against the driving direction!
     *
     * At low speeds the rolling resistance is the main resistance force, at high speeds the drag takes
     * over in magnitude. At approx. 100 km/h (30 m/s) they are equal ([Zuvich]). This means that
     * [AbstractCar.cRollingResistance] must be approximately 30 times the value of [AbstractCar.cDrag].
     */
    private fun calculateRollingResistance() {
        rollingResistance.set(cRollingResistance.toDouble()).times(velocity)
    }

    /**
     * Estimates the [AbstractCar.cRollingResistance] constant used for the rolling resistance calculation. THE CONSTANT
     * MAY BE FINETUNED!
     */
    private fun estimateCRollingResistance() {
        cRollingResistance = 50 * cDrag
    }

    /**
     * The (aerodynamic) drag (air-resistance) for the current velocity. Pointing against the driving direction! This
     * force is important because it is proportional to the square of the velocity. When we're driving fast this becomes
     * the most important resistance force.
     */
    private fun calculateDrag() {
        drag.set(cDrag.toDouble()).times(velocity).times(velocity.length())
    }

    /** Estimates the [AbstractCar.cDrag] constant used for the drag calculation. */
    private fun estimateCDrag() {
        cDrag = if (world != null) {
            15f * cDragFriction * frontalArea * world!!.airDensity(transform.translation.z)
        } else {
            15f * cDragFriction * frontalArea * AIR_DENSITY_BACKUP
        }
    }

    companion object {
        /**
         * The density of air at 0 degrees celsius in [kg/m^3]. Used for the drag estimation if not [AbstractCar.world]
         * is set.
         */
        private const val AIR_DENSITY_BACKUP = 1.2922f
    }

}
