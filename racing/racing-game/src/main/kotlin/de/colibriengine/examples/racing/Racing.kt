package de.colibriengine.examples.racing

import de.colibriengine.ColibriApplication
import de.colibriengine.ColibriEngine
import de.colibriengine.input.FECommands
import de.colibriengine.input.JoystickInputCallbacks
import de.colibriengine.input.MouseKeyboardInputCallbacks
import de.colibriengine.util.Version
import org.koin.core.component.KoinComponent
import org.koin.core.component.get

class Racing : KoinComponent, ColibriApplication, MouseKeyboardInputCallbacks, JoystickInputCallbacks {

    private val appName = "Racing - v.${Version(0, 0, 0, 9)}"
    private val engine: ColibriEngine = get()

    init {
        engine.inputManager.addMKInputCallback(this)
        engine.inputManager.addJInputCallback(this)
        engine.terminateWithoutWindows = true
        engine.windowManager.onlyProcessActiveWindow = false
        // Method blocks until the engine terminates.
        engine.start(this)
    }

    override fun onAppStart() {
        // Create worker and add them to the engine.
        engine.workerManager.addWorker(RacingGameWorker(engine))

        // Try to open a new OVRSession.
        // final OvrSessionImpl session = engine.getOvrSessionManager().createSession();
    }

    override fun onAppLoopStarted() {}
    override fun onAppLoopStopped() {}
    override fun onAppUpdate() {}
    override fun onAppTermination() {}

    override fun mkKeyCallback(key: Int, scancode: Int, action: Int, mods: Int) {
        if (key == FECommands.CLOSE_COMMAND.kKey && action == FECommands.CLOSE_COMMAND.kKeyAction) {
            engine.shouldTerminate = true
        }
        if (key == FECommands.TOGGLE_FULLSCREEN.kKey && action == FECommands.TOGGLE_FULLSCREEN.kKeyAction) {
            engine.windowManager.activeWindow!!.toggleFullscreen()
        }
    }

    override fun mkMoveCallback(dx: Int, dy: Int) {}
    override fun mkButtonCallback(button: Int, action: Int, mods: Int) {}
    override fun jAxisCallback(axis: Int, axisValue: Float) {}
    override fun jButtonCallback(button: Int, action: Int) {}
    override fun jLostConnectionCallback(index: Int) {}

}
