package de.colibriengine.examples.racing.model

import de.colibriengine.graphics.components.RenderComponent
import de.colibriengine.ecs.Entity
import de.colibriengine.examples.racing.gameobjects.AbstractCar
import de.colibriengine.math.vector.vec3f.StdVec3f
import de.colibriengine.graphics.model.ModelManager
import de.colibriengine.util.Timer

class Fiesta(entity: Entity, modelManager: ModelManager) : AbstractCar(entity) {

    init {
        entity.add(RenderComponent(modelManager.requestModel("models/fgk_highlod/fgk_highlod.obj")))

        /* SETUP CAR DATA */mass = 1200f
        centerOfGravity = StdVec3f(0f, 0f, 1f)
        wheelbase = 1.75f
        wheelRadius = 0.35f
        forwardGears = 5
        forwardGearRatios = floatArrayOf(2.66f, 1.78f, 1.30f, 1.0f, 0.74f)
        reverseGears = 1
        reverseGearRatios = floatArrayOf(2.90f)
        frontalArea = 2.2f
        enginePower = 8000f
        cBreakPower = 6000f
        cDragFriction = 0.3f

        // Start the engine :)
        isRunning = true
    }

    override fun init() {}
    override fun update(timing: Timer.View) {
        updateCarData(timing)
    }

    override fun destroy() {}

}
