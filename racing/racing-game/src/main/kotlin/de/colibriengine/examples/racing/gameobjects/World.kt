package de.colibriengine.examples.racing.gameobjects

import de.colibriengine.physics.BASE_TEMPERATURE
import de.colibriengine.physics.Planet

class World {

    var planet: Planet = Planet.EARTH

    /** The current temperature in [degrees celsius]. */
    var temperature: Float = BASE_TEMPERATURE

    fun airDensity(height: Float): Float {
        return planet.airDensity(height, temperature)
    }

}
