package de.colibriengine.examples.racing

import de.colibriengine.ColibriEngine
import de.colibriengine.worker.concrete.AbstractSimpleWorker

class RacingGameWorker(engine: ColibriEngine) : AbstractSimpleWorker(engine) {

    override fun onAdd() {
        super.onAdd()

        setCurrentScene(CarTest(engine, window))
    }

}
