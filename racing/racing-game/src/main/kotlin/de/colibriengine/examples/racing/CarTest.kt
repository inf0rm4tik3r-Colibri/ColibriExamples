package de.colibriengine.examples.racing

import de.colibriengine.ColibriEngine
import de.colibriengine.asset.ResourceName
import de.colibriengine.examples.racing.gameobjects.World
import de.colibriengine.examples.racing.model.Fiesta
import de.colibriengine.graphics.font.BitmapFont
import de.colibriengine.graphics.font.Text3D
import de.colibriengine.graphics.font.TextFactoryImpl
import de.colibriengine.graphics.font.TextMode
import de.colibriengine.graphics.icons.LightIcon
import de.colibriengine.graphics.lighting.light.DirectionalLight
import de.colibriengine.graphics.lighting.light.LightFactory
import de.colibriengine.graphics.lighting.light.LightFactoryImpl
import de.colibriengine.graphics.lighting.light.PointLight
import de.colibriengine.graphics.model.ModelNode
import de.colibriengine.graphics.model.sceneentities.TexturedQuad
import de.colibriengine.graphics.window.Window
import de.colibriengine.input.JoystickInputCallbacks
import de.colibriengine.input.MouseKeyboardInputCallbacks
import de.colibriengine.math.vector.vec3f.StdVec3f
import de.colibriengine.math.vector.vec3f.StdVec3fFactory.Companion.UNIT_X_AXIS
import de.colibriengine.math.vector.vec3f.StdVec3fFactory.Companion.UNIT_Y_AXIS
import de.colibriengine.scene.AbstractScene
import de.colibriengine.util.Timer

class CarTest(engine: ColibriEngine, window: Window) : AbstractScene(engine.ecs.createEntity()),
    MouseKeyboardInputCallbacks, JoystickInputCallbacks {
    private val world: World

    /** The player of the game. Has its own camera('s). */
    private val player: Player
    private val texturedQuad: TexturedQuad
    private val fiesta: Fiesta
    private val font: BitmapFont
    private val textEntity: Text3D
    private val directionalLight: DirectionalLight
    private val pointLight: PointLight

    private val textFactory = TextFactoryImpl(window.modelManager.modelFactory.meshFactory)

    private val lightFactory: LightFactory = LightFactoryImpl(
        engine.ecs,
        engine.windowManager,
        LightIcon(window.modelManager.modelFactory, window.textureManager)
    )

    init {
        val ecs = engine.ecs
        val modelManager = window.modelManager

        world = World()

        add(
            ModelNode(
                ecs.createEntity(), modelManager.modelFactory.coordinateSystem(
                    StdVec3f().set(0f, 0f, 0f), 0f, 6f, 0.5f, 5.0f
                )
            )
        )
        texturedQuad = TexturedQuad(
            ecs.createEntity(),
            window.modelManager.modelFactory,
            window.textureManager,
            "textures/grey_dotted.png",
            500
        )
        texturedQuad.transform.setTranslation(0f, 0f, 0f)
        texturedQuad.transform.rotation.initRotation(UNIT_X_AXIS, 0f)
        texturedQuad.transform.scale.set(500f)
        add(texturedQuad)
        fiesta = Fiesta(ecs.createEntity(), window.modelManager)
        fiesta.transform.scale.set(0.06f) //.setTranslation(0f, -0.04f, 0f);//; // TODO: shrink model in blender!
        fiesta.transform.rotation.initRotation(UNIT_Y_AXIS, 90f)
        fiesta.world = world
        add(fiesta)

        font = engine.fontManager.requestFont(ResourceName("fonts/jetbrains_128.bin.fnt"))
        textEntity = Text3D(ecs.createEntity(), textFactory, TextMode.FIXED, 7u, font)
        textEntity.text.update("CarTest")
        textEntity.transform.setTranslation(0f, 5.0f, 0f).scale.set(0.01f)
        add(textEntity)

        directionalLight = lightFactory.directional()
        directionalLight.transform.rotation.initRotation(UNIT_X_AXIS, 50f)
        directionalLight.transform.rotate(UNIT_Y_AXIS, 40f)
        directionalLight.transform.setTranslation(0f, 8f, 2f)
        directionalLight.color.set(1.0f, 1.0f, 1.0f)
        directionalLight.castsShadows = true
        //directionalLight.getVarianceShadowMap().setSettings();
        add(directionalLight)

        pointLight = lightFactory.point()
        pointLight.transform.setTranslation(0.0f, 4.5f, 0.0f)
        pointLight.color.set(1.0f, 1.0f, 1.0f)
        pointLight.attenuation.set(0.1f, 0.0f, 1.0f)
        pointLight.dropOffFactor = 1000f
        //add(pointLight);

        /* INIT CAMERA */
        player = Player(ecs, engine.inputManager, window)
        player.setInControlOfCar(fiesta)
        player.camera.use()
        //player.getCamera().getTransform().setTranslation(-20, 3, -20);
        player.camera.transform.setTranslation(5f, 3f, 5f)
        add(player.camera)
        add(player)
    }

    override fun destroyScene() {
    }

    override fun init() {}
    override fun update(timing: Timer.View) {}
    override fun destroy() {}

    override fun jAxisCallback(axis: Int, axisValue: Float) {}
    override fun jButtonCallback(button: Int, action: Int) {}
    override fun jLostConnectionCallback(index: Int) {}
    override fun mkMoveCallback(dx: Int, dy: Int) {}
    override fun mkButtonCallback(button: Int, action: Int, mods: Int) {}
    override fun mkKeyCallback(key: Int, scancode: Int, action: Int, mods: Int) {}

}
