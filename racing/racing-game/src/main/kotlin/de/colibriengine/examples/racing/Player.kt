package de.colibriengine.examples.racing

import de.colibriengine.ecs.ECS
import de.colibriengine.examples.racing.gameobjects.AbstractCar
import de.colibriengine.graphics.camera.AbstractCamera
import de.colibriengine.graphics.camera.concrete.ActionCameraImpl
import de.colibriengine.graphics.window.Window
import de.colibriengine.input.InputManager
import de.colibriengine.input.MouseKeyboardInputCallbacks
import de.colibriengine.math.quaternion.quaternionF.QuaternionF
import de.colibriengine.math.quaternion.quaternionF.StdQuaternionF
import de.colibriengine.math.vector.vec3f.StdVec3f
import de.colibriengine.math.vector.vec3f.Vec3f
import de.colibriengine.scene.graph.AbstractSceneGraphNode
import de.colibriengine.util.Timer
import org.lwjgl.glfw.GLFW

class Player(ecs: ECS, inputManager: InputManager, window: Window) :
    AbstractSceneGraphNode(ecs.createEntity()), MouseKeyboardInputCallbacks {

    //val camera: ArcBallCameraImpl = ArcBallCameraImpl(ecs.createEntity(), window)
    val camera: ActionCameraImpl = ActionCameraImpl(ecs.createEntity(), window)
    private var carInControl: AbstractCar?
    private var cameraFollowsCar: Boolean

    init {
        camera.position.set(0f, 2f, 4f)
        //camera.lookAt.set(5f, 1f, 2f)
        //camera.poleThresholdInDegrees = 10f
        inputManager.addMKInputCallback(camera)
        inputManager.addJInputCallback(camera)

        carInControl = null
        cameraFollowsCar = false

        inputManager.addMKInputCallback(this)
    }

    fun getCamera(): AbstractCamera {
        return camera
    }

    fun setInControlOfCar(car: AbstractCar?) {
        carInControl = car
    }

    override fun mkPosCallback(x: Int, y: Int) {}
    override fun mkMoveCallback(dx: Int, dy: Int) {}
    override fun mkButtonCallback(button: Int, action: Int, mods: Int) {}
    override fun mkKeyCallback(key: Int, scancode: Int, action: Int, mods: Int) {
        if (key == GLFW.GLFW_KEY_F) {
            if (action == GLFW.GLFW_PRESS) {
                cameraFollowsCar = !cameraFollowsCar
                //camera.setUpdatesEnabled(!cameraFollowsCar);
            }
        }
        if (carInControl != null) when (key) {
            GLFW.GLFW_KEY_UP -> when (action) {
                GLFW.GLFW_PRESS, GLFW.GLFW_REPEAT -> carInControl!!.gasPedalPosition = 1f
                else -> carInControl!!.gasPedalPosition = 0f
            }
            GLFW.GLFW_KEY_DOWN -> when (action) {
                GLFW.GLFW_PRESS, GLFW.GLFW_REPEAT -> carInControl!!.breakPedalPosition = 1f
                else -> carInControl!!.breakPedalPosition = 0f
            }
            GLFW.GLFW_KEY_LEFT -> when (action) {
                GLFW.GLFW_PRESS, GLFW.GLFW_REPEAT -> carInControl!!.steeringWheelPosition = (-1).toFloat()
                else -> carInControl!!.steeringWheelPosition = 0f
            }
            GLFW.GLFW_KEY_RIGHT -> when (action) {
                GLFW.GLFW_PRESS, GLFW.GLFW_REPEAT -> carInControl!!.steeringWheelPosition = 1f
                else -> carInControl!!.steeringWheelPosition = 0f
            }
            else -> {
                // Do nothing..
            }
        }
    }

    override fun init() {}

    private val prevCarPos: Vec3f = StdVec3f()
    private val newCarPos: Vec3f = StdVec3f()
    private val prevCamPos: Vec3f = StdVec3f()
    private val carPosDif: Vec3f = StdVec3f()

    override fun update(timing: Timer.View) {
        if (carInControl != null && cameraFollowsCar) {
            prevCarPos.set(carInControl!!.transform.translation)
            prevCamPos.set(camera.transform.translation)

            // Update the camera
/*
            camera.getTransform().setTranslation(
                    newCamPos.set(carInControl.getTransform().getTranslation())
                            .plus(carBackDir.times(10.0f))
            );
*/
            // Position difference.
            carPosDif.set(prevCarPos).minus(newCarPos)
            // Try to follow the car.
            camera.transform.translation.plus(carPosDif)

            // Catch up with position?
            /*
            carCamDif.set(prevCarPos).minus(prevCamPos);
            if (carCamDif.length() > 5) {
                camera.getTransform().translate(carCamDif.times(0.2f));
            }
            */
            val pointBehindCar: Vec3f = StdVec3f().set(carInControl!!.transform.translation)
                .minus(carInControl!!.transformCalculation.forward(StdVec3f()).times(4f))
            pointBehindCar.y = pointBehindCar.y + 1.75f

            // Difference to point behind car.
            val diffToPbc: Vec3f = StdVec3f().set(pointBehindCar).minus(prevCamPos)
            camera.transform.translation.plus(diffToPbc.times(30f * timing.deltaSP()))


            // Difference in orientation!
            val carCamRotDif: QuaternionF = StdQuaternionF()
            val carCamRotDif2: QuaternionF = StdQuaternionF()
            carCamRotDif
                .set(camera.transform.rotation).normalize()
                .conjugate()
                .times(carCamRotDif2.set(carInControl!!.transform.rotation).normalize().invert())
            // why invert?
            //camera.getTransform().rotate(carCamRotDif);

            // Catch up with orientation.
            val carRot = StdQuaternionF().set(carInControl!!.transform.rotation).normalize().invert()
            camera.transform.rotate(carRot, 2 * timing.deltaSP())
            newCarPos.set(carInControl!!.transform.translation)
        }
    }

    override fun destroy() {}

}
